﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using Ssepan.Utility.Core;
using Ssepan.Application.Core;
using Ssepan.Application.Core.EtoForms;
using MvcLibrary.Core;
using Eto.Forms;
using Eto.Drawing;

namespace MvcForms.Core.EtoForms
{
	/// <summary>
	/// Note: this class can subclass the base without type parameters.
	/// </summary>
	public class MVCViewModel :
        FormsViewModel<Image, MVCSettings, MVCModel, MvcView>
    {
        #region Constructors
        public MVCViewModel() { }//Note: not called, but need to be present to compile--SJS

        public MVCViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            Dictionary<string, Image> actionIconImages,
            FileDialogInfo<Window, DialogResult> settingsFileDialogInfo
        ) :
            base(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo)
        {
            try
            {
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        public MVCViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            Dictionary<string, Image> actionIconImages,
            FileDialogInfo<Window, DialogResult> settingsFileDialogInfo,
            MvcView view
        ) :
            base(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo, view)
        {
            try
            {
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// model specific, not generic
        /// </summary>
        internal void DoSomething()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Doing something...",
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                ModelController<MVCModel>.Model.SomeBoolean = !ModelController<MVCModel>.Model.SomeBoolean;
				ModelController<MVCModel>.Model.SomeInt++;
                ModelController<MVCModel>.Model.SomeString = DateTime.Now.ToString();

                ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean = !ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean;
				ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt++;
                ModelController<MVCModel>.Model.SomeComponent.SomeOtherString = DateTime.Now.ToString();

                ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean = !ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean;
				ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt++;
                ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString = DateTime.Now.ToString();
                //TODO:implement custom messages
                UpdateStatusBarMessages(null, null, DateTime.Now.ToLongTimeString());

                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
            finally
            {
                StopProgressBar("Did something.");
            }
        }

        //override base
        public override void EditPreferences()
        {
            string errorMessage = null;
            FileDialogInfo<Window, DialogResult> fileDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Preferences...",
                    null,
                    _actionIconImages["Preferences"],
                    true,
                    33
                );

                //get folder here
                fileDialogInfo = new FileDialogInfo<Window, DialogResult>
                (
                    View,
                    true,
                    "Select Folder...",
                    DialogResult.None
                );
                if (!Dialogs.GetFolderPath(ref fileDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(string.Format("GetFolderPath: {0}", errorMessage));
                }

                if (fileDialogInfo.Response != DialogResult.None)
                {
                    if (fileDialogInfo.Response == DialogResult.Ok)
                    {
                        UpdateStatusBarMessages(StatusMessage + fileDialogInfo.Filename + ACTION_IN_PROGRESS, null);
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if open was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if open was cancelled
                }

                //override base, which did this
                // if (!Preferences())
                // {
                //     throw new ApplicationException("'Preferences' error");
                // }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("Preferences failed: {0}", ex.Message));
            }
        }

        public /*override*/ void HelpAbout<TAssemblyInfo>()
            where TAssemblyInfo :
            //class,
            AssemblyInfoBase<Window>,
            new()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;
            TAssemblyInfo assemblyInfo = null;
            AboutDialogInfo<Window, DialogResult, Image> aboutDialogInfo = null;
            string errorMessage = null;

            try
            {
                StartProgressBar
                (
                    "About" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["About"],
                    true,
                    33
                );

                //assemblyInfo NOT passed in;specifically, NOT called by override of HelpAbout<TAssemblyInfo>()
                assemblyInfo = new TAssemblyInfo();
                Debug.Assert(View != null);

                // use GUI mode About feature
                aboutDialogInfo =
                    new AboutDialogInfo<Window, DialogResult, Image>
					(
						parent: View,
						modal: true,
						title: "About " + assemblyInfo.Title,
						response: DialogResult.None,
						programName: assemblyInfo.Product,
						version: assemblyInfo.Version,
						copyright: assemblyInfo.Copyright,
						comments: assemblyInfo.Description,
						website: assemblyInfo.Website,
						//TODO:use logo from loaded resources;this code only works because this lib is run from the context of the calling app
						logo: View.imageResources["App"],//GetIconFromBitmap(GetBitmapFromFile("../MvcForms.Core.EtoForms/Resources/App.png"), 32, 32),//TODO:this path is not xplat compatible
						websiteLabel: assemblyInfo.WebsiteLabel,
						designers: assemblyInfo.Designers,
						developers: assemblyInfo.Developers,
						documenters: assemblyInfo.Documenters,
						license: assemblyInfo.License
					);

                if (!Dialogs.ShowAboutDialog(ref aboutDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(errorMessage);
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        /// <summary>
        /// get font name
        /// </summary>
        public void GetFont()
        {
            string errorMessage = null;
            FontDialogInfo<Window, DialogResult, Font> fontDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Font" + ACTION_IN_PROGRESS,
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                //Note:must be set or gets null ref error after dialog closes
                Font fontDescription = new(FontFamilies.Sans, 10);
                fontDialogInfo = new FontDialogInfo<Window, DialogResult, Font>
                (
                    parent: View,
                    modal: true,
                    title: "Select Font",
                    response: DialogResult.None,
                    fontDescription: fontDescription
                );

                if (!Dialogs.GetFont(ref fontDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(string.Format("GetFont: {0}", errorMessage));
                }

                if (fontDialogInfo.Response != DialogResult.None)
                {
                    if (fontDialogInfo.Response == DialogResult.Ok)
                    {
                        UpdateStatusBarMessages(StatusMessage + fontDialogInfo.FontDescription + ACTION_IN_PROGRESS, null);
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if open was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if open was cancelled
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        /// <summary>
        /// get color RGB
        /// </summary>
        public void GetColor()
        {
            string errorMessage = null;
            ColorDialogInfo<Window, DialogResult, Color> colorDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Color" + ACTION_IN_PROGRESS,
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                Color color = Color.FromArgb(0, 0, 0);
                colorDialogInfo = new ColorDialogInfo<Window, DialogResult, Color>
                (
                    View,
                    true,
                    "Select Color",
                    DialogResult.None,
                    color
                );

                if (!Dialogs.GetColor(ref colorDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(string.Format("GetColor: {0}", errorMessage));
                }

                if (colorDialogInfo.Response != DialogResult.None)
                {
                    if (colorDialogInfo.Response == DialogResult.Ok)
                    {
                        UpdateStatusBarMessages(StatusMessage + colorDialogInfo.Color + ACTION_IN_PROGRESS, null);
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if open was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if open was cancelled
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        #endregion Methods

    }
}
