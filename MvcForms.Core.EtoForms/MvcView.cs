using System;
using System.ComponentModel;
using System.Collections.Generic;
// using System.Drawing;
// using System.Drawing.Drawing2D;
// using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using Ssepan.Utility.Core;
using Ssepan.Io.Core;
using Ssepan.Application.Core;
using Ssepan.Application.Core.EtoForms;
using MvcLibrary.Core;
using Eto.Forms;
using Eto.Drawing;

namespace MvcForms.Core.EtoForms
{
	public partial class MvcView :
        Form,
        INotifyPropertyChanged
    {
        #region Declarations
        protected bool disposed;
        // private bool _ValueChangedProgrammatically;
        private const string IMAGE_RESOURCE_PATH = "../MvcForms.Core.EtoForms/Resources/{0}.png";//TODO:this is NOT xplat compatible!

        internal Dictionary<string, Image> imageResources;

        //cancellation hook
        // System.Action cancelDelegate = null;

        protected MVCViewModel ViewModel;

        #endregion Declarations

        #region Constructors    
        public MvcView()
        {
            try
            {
                InitializeResources();

                InitializeComponent();

                //Not shown in titlebar, but visible in Alt-Tab
                //TODO:Set Icon "./Resources/App.png"

                statusMessage.Text = "";
                errorMessage.Text = "";

                ////(re)define default output delegate
                //ConsoleApplication.defaultOutputDelegate = ConsoleApplication.messageBoxWrapperOutputDelegate;

                //subscribe to view's notifications
                if (PropertyChanged != null)
                {
                    PropertyChanged += PropertyChangedEventHandlerDelegate;
                }

                InitViewModel();

                BindSizeAndLocation();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        private void InitializeResources()
        {
            imageResources =
                new Dictionary<string, Image>()
                { //TODO:ideally, should get these from resource, but items must be generated into resource class.
                    { "App", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"App")), 32, 32) },
                    { "New", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"New")), 22, 22) },
                    { "Open", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Open")), 22, 22) },
                    { "Save", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Save")), 22, 22) },
                    { "Print", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Print")), 22, 22) },
                    { "Undo", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Undo")), 22, 22) },
                    { "Redo", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Redo")), 22, 22) },
                    { "Cut", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Cut")), 22, 22) },
                    { "Copy", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Copy")), 22, 22) },
                    { "Paste", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Paste")), 22, 22) },
                    { "Delete", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Delete")), 22, 22) },
                    { "Find", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Find")), 22, 22) },
                    { "Replace", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Replace")), 22, 22) },
                    { "Refresh", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Reload")), 22, 22) },
                    { "Preferences", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Preferences")), 22, 22) },
                    { "Properties", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Properties")), 22, 22) },
                    { "Contents", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Contents")), 22, 22) },
                    { "About", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"About")), 22, 22) }
                };
        }
        #endregion Constructors

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            try
            {
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
			}
            catch (Exception ex)
            {
                ViewModel.ErrorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion INotifyPropertyChanged

        #region Properties
        private string _ViewName = ProgramBase.APP_NAME;
        public string ViewName
        {
            get { return _ViewName; }
            set
            {
                _ViewName = value;
                OnPropertyChanged(nameof(ViewName));
            }
        }
        #endregion Properties

        #region Handlers

        #region PropertyChangedEventHandlerDelegates
        /// <summary>
        /// Note: model property changes update UI manually.
        /// Note: handle settings property changes manually.
        /// Note: because settings properties are a subset of the model
        ///  (every settings property should be in the model,
        ///  but not every model property is persisted to settings)
        ///  it is decided that for now the settings handler will
        ///  invoke the model handler as well.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">PropertyChangedEventArgs</param>
        protected void PropertyChangedEventHandlerDelegate(object sender, PropertyChangedEventArgs e)
        {
            try
            {
                #region Model
                if (e.PropertyName == "IsChanged")
                {
                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", e.PropertyName));
                    ApplySettings();
                }
                //Status Bar
                else if (e.PropertyName == "ActionIconIsVisible")
                {
                    actionIcon.Visible = ViewModel.ActionIconIsVisible;
                }
                else if (e.PropertyName == "ActionIconImage")
                {
                    actionIcon.Image = (ViewModel?.ActionIconImage);
                }
                else if (e.PropertyName == "StatusMessage")
                {
                    //replace default action by setting control property
                    //skip status message updates after Viewmodel is null
                    statusMessage.Text = (ViewModel?.StatusMessage);
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", StatusMessage));
                }
                else if (e.PropertyName == "ErrorMessage")
                {
                    //replace default action by setting control property
                    //skip status message updates after Viewmodel is null
                    errorMessage.Text = (ViewModel?.ErrorMessage);
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", ErrorMessage));
                }
                else if (e.PropertyName == "CustomMessage")
                {
                    //replace default action by setting control property
                    //StatusBarCustomMessage.Text = ViewModel.CustomMessage;
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", ErrorMessage));
                }
                else if (e.PropertyName == "ErrorMessageToolTipText")
                {
                    errorMessage.ToolTip = ViewModel.ErrorMessageToolTipText;
                }
                else if (e.PropertyName == "ProgressBarValue")
                {
                    progressBar.Value = ViewModel.ProgressBarValue;
                }
                else if (e.PropertyName == "ProgressBarMaximum")
                {
                    progressBar.MaxValue = ViewModel.ProgressBarMaximum;
                }
                else if (e.PropertyName == "ProgressBarMinimum")
                {
                    progressBar.MinValue = ViewModel.ProgressBarMinimum;
                }
                else if (e.PropertyName == "ProgressBarStep")
                {
                    // progressBar.PulseStep = ViewModel.ProgressBarStep;
                }
                else if (e.PropertyName == "ProgressBarIsMarquee")
                {
                    progressBar.Indeterminate = ViewModel.ProgressBarIsMarquee;
                }
                else if (e.PropertyName == "ProgressBarIsVisible")
                {
                    progressBar.Visible = ViewModel.ProgressBarIsVisible;
                }
                else if (e.PropertyName == "DirtyIconIsVisible")
                {
                    dirtyIcon.Visible = ViewModel.DirtyIconIsVisible;
                }
                else if (e.PropertyName == "DirtyIconImage")
                {
                    dirtyIcon.Image = ViewModel.DirtyIconImage;
                }
                //use if properties cannot be databound
                else if (e.PropertyName == "SomeInt")
                {//TODO:research binding on this platform (Eto.Forms)
                   txtSomeInt.Text = ModelController<MVCModel>.Model.SomeInt.ToString();
                }
                else if (e.PropertyName == "SomeBoolean")
                {
                   chkSomeBoolean.Checked = ModelController<MVCModel>.Model.SomeBoolean;
                }
                else if (e.PropertyName == "SomeString")
                {
                   txtSomeString.Text = ModelController<MVCModel>.Model.SomeString;
                }
                else if (e.PropertyName == "StillAnotherInt")
                {
                   txtStillAnotherInt.Text = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt.ToString();
                }
                else if (e.PropertyName == "StillAnotherBoolean")
                {
                   chkStillAnotherBoolean.Checked = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean;
                }
                else if (e.PropertyName == "StillAnotherString")
                {
                   txtStillAnotherString.Text = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString;
                }
                else if (e.PropertyName == "SomeOtherInt")
                {
                   txtSomeOtherInt.Text = ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt.ToString();
                }
                else if (e.PropertyName == "SomeOtherBoolean")
                {
                   chkSomeOtherBoolean.Checked = ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean;
                }
                else if (e.PropertyName == "SomeOtherString")
                {
                   txtSomeOtherString.Text = ModelController<MVCModel>.Model.SomeComponent.SomeOtherString;
                }
                //else if (e.PropertyName == "SomeComponent")
                //{
                //    ConsoleApplication.defaultOutputDelegate(string.Format("SomeComponent: {0},{1},{2}", ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt, ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean, ModelController<MVCModel>.Model.SomeComponent.SomeOtherString));
                //}
                //else if (e.PropertyName == "StillAnotherComponent")
                //{
                //    ConsoleApplication.defaultOutputDelegate(string.Format("StillAnotherComponent: {0},{1},{2}", ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt, ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean, ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString));
                //}
                else
                {
                    #if DEBUG_MODEL_PROPERTYCHANGED
                    ConsoleApplication.defaultOutputDelegate(string.Format("e.PropertyName: {0}", e.PropertyName));
                    #endif
                }
                #endregion Model

                #region Settings
                if (e.PropertyName == "Dirty")
                {
                    //apply settings that don't have databindings
                    ViewModel.DirtyIconIsVisible = SettingsController<MVCSettings>.Settings.Dirty;
                }
                else
                {
                    #if DEBUG_SETTINGS_PROPERTYCHANGED
                    ConsoleApplication.defaultOutputDelegate(string.Format("e.PropertyName: {0}", e.PropertyName));
                    #endif
                }
                #endregion Settings
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        #endregion PropertyChangedEventHandlerDelegates

        #region Form
        private void MvcView_Load(object sender, EventArgs e)
        {
            try
            {
                ViewModel.StatusMessage = string.Format("{0} starting...", ViewName);

                ViewModel.StatusMessage = string.Format("{0} started.", ViewName);

                //_Run();//only called here in console apps; use button in forms apps
            }
            catch (Exception ex)
            {
                ViewModel.ErrorMessage = ex.Message;
                ViewModel.StatusMessage = string.Empty;

                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        private void MvcView_Closing(object sender, EventArgs e)
        {
            bool resultDontQuit = false;

            try
            {
                ViewModel.FileExit(ref resultDontQuit);

                ((CancelEventArgs)e).Cancel = resultDontQuit;

                if (!resultDontQuit)
                {
                    //clean up data model here
                    ViewModel.StatusMessage = string.Format("{0} completing...", ViewName);
                    DisposeSettings();
                    ViewModel.StatusMessage = string.Format("{0} completed.", ViewName);

                    ViewModel = null;

                    //Application.Instance.Quit();//this will only re-trigger the Closing event; just allow window to close
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                if (ViewModel != null)
                {
                    ViewModel.ErrorMessage = ex.Message;
                }
            }
        }
        #endregion Form

        #region Controls
        private void TxtSomeInt_TextChanged(object sender, EventArgs e)
        {
			ModelController<MVCModel>.Model.SomeInt =
				int.TryParse(txtSomeInt.Text, out int result) ? result : 0;
		}

        private void TxtSomeOtherInt_TextChanged(object sender, EventArgs e)
        {
			ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt =
				int.TryParse(txtSomeOtherInt.Text, out int result) ? result : 0;
		}

        private void TxtStillAnotherInt_TextChanged(object sender, EventArgs e)
        {
			ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt =
				int.TryParse(txtStillAnotherInt.Text, out int result) ? result : 0;
		}

        private void TxtSomeString_TextChanged(object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.SomeString = txtSomeString.Text;
        }

        private void TxtSomeOtherString_TextChanged(object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.SomeComponent.SomeOtherString = txtSomeOtherString.Text;
        }

        private void TxtStillAnotherString_TextChanged(object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString = txtStillAnotherString.Text;
        }

        private void ChkSomeBoolean_CheckChanged(object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.SomeBoolean = chkSomeBoolean.Checked.Value;
        }

        private void ChkSomeOtherBoolean_CheckChanged(object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean = chkSomeOtherBoolean.Checked.Value;
        }

        private void ChkStillAnotherBoolean_CheckChanged(object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean = chkStillAnotherBoolean.Checked.Value;
        }

        private void CmdRun_Clicked(object sender, EventArgs e)
        {
            //do something
            ViewModel.DoSomething();
        }
        private void CmdColor_Click(object sender, EventArgs e)
		{
            ViewModel.GetColor();
        }
        private void CmdFont_Click(object sender, EventArgs e)
		{
            ViewModel.GetFont();
        }
        #endregion Controls

        #region Menu
        private void MenuFileNew_Click(object sender, EventArgs e)
		{
            ViewModel.FileNew();
        }
        private void MenuFileOpen_Click(object sender, EventArgs e)
		{
            ViewModel.FileOpen();
        }
        private void MenuFileSave_Click(object sender, EventArgs e)
		{
            ViewModel.FileSave();
        }
        private void MenuFileSaveAs_Click(object sender, EventArgs e)
		{
            ViewModel.FileSaveAs();
        }
        private void MenuFilePrint_Click(object sender, EventArgs e)
		{
            ViewModel.FilePrint();
        }
        private void MenuFilePrintPreview_Click(object sender, EventArgs e)
		{
            ViewModel.FilePrintPreview();
        }
        private void MenuFileQuit_Click(object sender, EventArgs e)
		{
            Close();
        }
        private void MenuEditUndo_Click(object sender, EventArgs e)
		{
            ViewModel.EditUndo();
        }
        private void MenuEditRedo_Click(object sender, EventArgs e)
		{
            ViewModel.EditRedo();
        }
        private void MenuEditSelectAll_Click(object sender, EventArgs e)
		{
            ViewModel.EditSelectAll();
        }
        private void MenuEditCut_Click(object sender, EventArgs e)
		{
            ViewModel.EditCut();
        }
        private void MenuEditCopy_Click(object sender, EventArgs e)
		{
            ViewModel.EditCopy();
        }
        private void MenuEditPaste_Click(object sender, EventArgs e)
		{
            ViewModel.EditPaste();
        }
        private void MenuEditPasteSpecial_Click(object sender, EventArgs e)
		{
            ViewModel.EditPasteSpecial();
        }
        private void MenuEditDelete_Click(object sender, EventArgs e)
		{
            ViewModel.EditDelete();
        }
        private void MenuEditFind_Click(object sender, EventArgs e)
		{
            ViewModel.EditFind();
        }
        private void MenuEditReplace_Click(object sender, EventArgs e)
		{
            ViewModel.EditReplace();
        }
        private void MenuEditRefresh_Click(object sender, EventArgs e)
		{
            ViewModel.EditRefresh();
        }
        private void MenuEditPreferences_Click(object sender, EventArgs e)
		{
            ViewModel.EditPreferences();
        }
        private void MenuEditProperties_Click(object sender, EventArgs e)
		{
            ViewModel.EditProperties();
        }
        private void MenuWindowNewWindow_Click(object sender, EventArgs e)
		{
            ViewModel.WindowNewWindow();
        }
        private void MenuWindowTile_Click(object sender, EventArgs e)
		{
            ViewModel.WindowTile();
        }
        private void MenuWindowCascade_Click(object sender, EventArgs e)
		{
            ViewModel.WindowCascade();
        }
        private void MenuWindowArrangeAll_Click(object sender, EventArgs e)
		{
            ViewModel.WindowArrangeAll();
        }
        private void MenuWindowHide_Click(object sender, EventArgs e)
		{
            ViewModel.WindowHide();
        }
        private void MenuWindowShow_Click(object sender, EventArgs e)
		{
            ViewModel.WindowShow();
        }
        private void MenuHelpContents_Click(object sender, EventArgs e)
		{
            ViewModel.HelpContents();
        }
        private void MenuHelpIndex_Click(object sender, EventArgs e)
		{
            ViewModel.HelpIndex();
        }
        private void MenuHelpOnlineHelp_Click(object sender, EventArgs e)
		{
            ViewModel.HelpOnlineHelp();
        }
        private void MenuHelpLicenceInformation_Click(object sender, EventArgs e)
		{
            ViewModel.HelpLicenceInformation();
        }
        private void MenuHelpCheckForUpdates_Click(object sender, EventArgs e)
		{
            ViewModel.HelpCheckForUpdates();
        }
        private void MenuHelpAbout_Click(object sender, EventArgs e)
        {
            ViewModel.HelpAbout<AssemblyInfo>();
        }
        #endregion Menu
        #endregion Handlers

        #region Methods
        #region FormAppBase
        protected void InitViewModel()
        {
            FileDialogInfo<Window, DialogResult> settingsFileDialogInfo = null;

            try
            {
                //tell controller how model should notify view about non-persisted properties AND including model properties that may be part of settings
                ModelController<MVCModel>.DefaultHandler = PropertyChangedEventHandlerDelegate;

                //tell controller how settings should notify view about persisted properties
                SettingsController<MVCSettings>.DefaultHandler = PropertyChangedEventHandlerDelegate;

				InitModelAndSettings();

				//settings used with file dialog interactions
				settingsFileDialogInfo =
					new FileDialogInfo<Window, DialogResult>
					(
						parent: this,
						modal: true,
						title: null,
						response: DialogResult.None,
						newFilename: SettingsController<MVCSettings>.FILE_NEW,
						filename: null,
						extension: SettingsBase.FileTypeExtension,
						description: SettingsBase.FileTypeDescription,
						typeName: SettingsBase.FileTypeName,
						additionalFilters:
						[
                            //"MvcSettings files (*.mvcsettings)|*.mvcsettings", //default (xml) defined in SettingsBase, overridden in MVCSettings
                            "JSON files (*.json)|*.json",
							"XML files (*.xml)|*.xml",
							"All files (*.*)|*.*"
						],
						multiselect: false,
						initialDirectory: default,
						forceDialog: false,
						forceNew: false,
						customInitialDirectory: Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator()
					)
					{
						//set dialog caption
						Title = Title
					};

				//class to handle standard behaviors
				ViewModelController<Image, MVCViewModel>.New
                (
                    ViewName,
                    new MVCViewModel
                    (
                        PropertyChangedEventHandlerDelegate,
                        new Dictionary<string, Image>()
                        { //TODO:ideally, should get these from library, but items added did not get generated into resource class.
                            { "App", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"App")), 32, 32) },
                            { "New", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"New")), 22, 22) },
                            { "Open", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Open")), 22, 22) },
                            { "Save", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Save")), 22, 22) },
                            { "Print", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Print")), 22, 22) },
                            { "Undo", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Undo")), 22, 22) },
                            { "Redo", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Redo")), 22, 22) },
                            { "Cut", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Cut")), 22, 22) },
                            { "Copy", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Copy")), 22, 22) },
                            { "Paste", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Paste")), 22, 22) },
                            { "Delete", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Delete")), 22, 22) },
                            { "Find", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Find")), 22, 22) },
                            { "Replace", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Replace")), 22, 22) },
                            { "Refresh", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Reload")), 22, 22) },
                            { "Preferences", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Preferences")), 22, 22) },
                            { "Properties", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Properties")), 22, 22) },
                            { "Contents", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Contents")), 22, 22) },
                            { "About", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"About")), 22, 22) }
                        },
                        settingsFileDialogInfo,
                        this
                    )
                );

                //select a viewmodel by view name
                ViewModel = ViewModelController<Image, MVCViewModel>.ViewModel[ViewName];

                // BindFormUi();

                //Init config parameters
                if (!LoadParameters())
                {
                    throw new Exception(string.Format("Unable to load config file parameter(s)."));
                }

                //Load
                if ((SettingsController<MVCSettings>.FilePath == null) || SettingsController<MVCSettings>.Filename.StartsWith(SettingsController<MVCSettings>.FILE_NEW))
                {
                    //NEW
                    ViewModel.FileNew();
                }
                else
                {
                    //OPEN
                    ViewModel.FileOpen(false);
                }

#if debug
            //debug view
            menuEditProperties_Click(sender, e);
#endif

                //Display dirty state
                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                if (ViewModel != null)
                {
                    ViewModel.ErrorMessage = ex.Message;
                }
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        protected static void InitModelAndSettings()
        {
            //create Settings before first use by Model
            if (SettingsController<MVCSettings>.Settings == null)
            {
                SettingsController<MVCSettings>.New();
            }
            //Model properties rely on Settings, so don't call Refresh before this is run.
            if (ModelController<MVCModel>.Model == null)
            {
                ModelController<MVCModel>.New();
            }
        }

        protected void DisposeSettings()
        {
            MessageDialogInfo<Window, DialogResult, object, MessageBoxType, MessageBoxButtons> questionMessageDialogInfo = null;
            string errorMessage = null;

			//save user and application settings 
			EtoForms.Properties.Settings.Default.Save();

            if (SettingsController<MVCSettings>.Settings.Dirty)
            {
                //prompt before saving
                questionMessageDialogInfo = new MessageDialogInfo<Window, DialogResult, object, MessageBoxType, MessageBoxButtons>
                (
                    parent: this,
                    modal: true,
                    title: Title,
                    dialogFlags: null,
                    messageType: MessageBoxType.Question,
                    buttonsType: MessageBoxButtons.YesNo,
                    message: "Save changes?",
                    response: DialogResult.None
                );
                if (!Dialogs.ShowMessageDialog(ref questionMessageDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(errorMessage);
                }

                // DialogResult dialogResult = MessageBox.Show("Save changes?", this.Title, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                switch (questionMessageDialogInfo.Response)
                {
					case DialogResult.Yes:
						//SAVE
						ViewModel.FileSave();

						break;

					case DialogResult.No:
						break;

					default:
						throw new InvalidEnumArgumentException();
				}
            }

            //unsubscribe from model notifications
            ModelController<MVCModel>.Model.PropertyChanged -= PropertyChangedEventHandlerDelegate;
        }

        protected static void Run()
        {
            //MessageBox.Show("running", "MVC", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        }
        #endregion FormAppBase

        #region Utility
        // /// <summary>
        // /// Bind static Model controls to Model Controller
        // /// </summary>
        // private void BindFormUi()
        // {
        //     try
        //     {
        //         //Form

        //         //Controls
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //         throw;
        //     }
        // }

        // /// <summary>
        // /// Bind Model controls to Model Controller
        // /// Note: databinding not use in EtoForms implementation, but leave this in place 
        // ///  in case you want to do this
        // /// </summary>
        // private void BindModelUi()
        // {
        //     try
        //     {
        //         BindField<TextBox, MVCModel>(txtSomeInt, ModelController<MVCModel>.Model, "SomeInt");
        //         BindField<TextBox, MVCModel>(txtSomeString, ModelController<MVCModel>.Model, "SomeString");
        //         BindField<CheckBox, MVCModel>(chkSomeBoolean, ModelController<MVCModel>.Model, "SomeBoolean", "Checked");

        //         BindField<TextBox, MVCModel>(txtSomeOtherInt, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherInt");
        //         BindField<TextBox, MVCModel>(txtSomeOtherString, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherString");
        //         BindField<CheckBox, MVCModel>(chkSomeOtherBoolean, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherBoolean", "Checked");

        //         BindField<TextBox, MVCModel>(txtStillAnotherInt, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherInt");
        //         BindField<TextBox, MVCModel>(txtStillAnotherString, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherString");
        //         BindField<CheckBox, MVCModel>(chkStillAnotherBoolean, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherBoolean", "Checked");
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //         throw;
        //     }
        // }

        // /// Note: databinding not use in EtoForms implementation, but leave this in place 
        // ///  in case you want to do this
        // private void BindField<TControl, TModel>
        // (
        //     TControl fieldControl,
        //     TModel model,
        //     string modelPropertyName,
        //     string controlPropertyName = "Text",
        //     bool formattingEnabled = false,
        //     //DataSourceUpdateMode dataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged,
        //     bool reBind = true
        // )
        //     where TControl : Control
        // {
        //     try
        //     {
        //         //TODO: .RemoveSignalHandler ?
        //         //fieldControl.DataBindings.Clear();
        //         if (reBind)
        //         {
        //             //TODO:.AddSignalHandler ?
        //             //fieldControl.DataBindings.Add(controlPropertyName, model, modelPropertyName, formattingEnabled, dataSourceUpdateMode);
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        /// <summary>
        /// Apply Settings to viewer.
        /// </summary>
        private void  ApplySettings()
        {
            try
            {
                // _ValueChangedProgrammatically = true;

                //apply settings that have databindings
                // BindModelUi();

                //apply settings that shouldn't use databindings

                //apply settings that can't use databindings
                Title = Path.GetFileName(SettingsController<MVCSettings>.Filename) + " - " + ViewName;

                //apply settings that don't have databindings
                ViewModel.DirtyIconIsVisible = SettingsController<MVCSettings>.Settings.Dirty;
                //update remaining non-bound controls: textboxes, checkboxes
                //Note: ModelController<MVCModel>.Model.Refresh() will cause SO here
                ModelController<MVCModel>.Model.Refresh("SomeInt");
                ModelController<MVCModel>.Model.Refresh("SomeBoolean");
                ModelController<MVCModel>.Model.Refresh("SomeString");
                ModelController<MVCModel>.Model.Refresh("SomeOtherInt");
                ModelController<MVCModel>.Model.Refresh("SomeOtherBoolean");
                ModelController<MVCModel>.Model.Refresh("SomeOtherString");
                ModelController<MVCModel>.Model.Refresh("StillAnotherInt");
                ModelController<MVCModel>.Model.Refresh("StillAnotherBoolean");
                ModelController<MVCModel>.Model.Refresh("StillAnotherString");

                // _ValueChangedProgrammatically = false;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        // /// <summary>
        // /// Set function button and menu to enable value, and cancel button to opposite.
        // /// For now, do only disabling here and leave enabling based on biz logic
        // ///  to be triggered by refresh?
        // /// Note: another feature that is not used, but could be
        // /// </summary>
        // /// <param name="functionButton">Button</param>
        // /// <param name="functionToolbarButton">Button</param>
        // /// <param name="functionMenu">MenuItem</param>
        // /// <param name="cancelButton">Button</param>
        // /// <param name="enable">bool</param>
        // private void SetFunctionControlsEnable
        // (
        //     Button functionButton,
        //     Button functionToolbarButton,
        //     MenuItem functionMenu,
        //     Button cancelButton,
        //     bool enable
        // )
        // {
        //     try
        //     {
        //         //stand-alone button
        //         if (functionButton != null)
        //         {
        //             functionButton.Enabled = enable;
        //         }

        //         //toolbar button
        //         if (functionToolbarButton != null)
        //         {
        //             functionToolbarButton.Enabled = enable;
        //         }

        //         //menu item
        //         if (functionMenu != null)
        //         {
        //             functionMenu.Enabled = enable;
        //         }

        //         //stand-alone cancel button
        //         if (cancelButton != null)
        //         {
        //             cancelButton.Enabled = !enable;
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        // /// <summary>
        // /// Invoke any delegate that has been registered 
        // ///  to cancel a long-running background process.
        // /// Note: another feature that is not used, but could be
        // /// </summary>
        // private void InvokeActionCancel()
        // {
        //     try
        //     {
        //         //execute cancellation hook
        //         if (cancelDelegate != null)
        //         {
        //             cancelDelegate();
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        /// <summary>
        /// Load from app config; override with command line if present
        /// </summary>
        /// <returns>bool</returns>
        private static bool LoadParameters()
        {
            bool returnValue = default;

            try
            {
                // First, get configured values

                //get filename from App.config
                if (!Configuration.ReadString("SettingsFilename", out string _settingsFilename))
                {
                    throw new ApplicationException(string.Format("Unable to load SettingsFilename: {0}", "SettingsFilename"));
                }
                if ((_settingsFilename == null) || (_settingsFilename == SettingsController<MVCSettings>.FILE_NEW))
                {
                    throw new ApplicationException(string.Format("Settings filename not set: '{0}'.\nCheck SettingsFilename in App.config file.", _settingsFilename));
                }
                //use with the supplied path
                SettingsController<MVCSettings>.Filename = _settingsFilename;

                //identify directory to specified, or use default
                if (Path.GetDirectoryName(_settingsFilename)?.Length == 0)
                {
                    //supply default path if missing
                    SettingsController<MVCSettings>.Pathname = Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator();
                }

                //get serialization format from App.config
                if (!Configuration.ReadString("SettingsSerializeAs", out string _settingsSerializeAs))
                {
                    throw new ApplicationException(string.Format("Unable to load SettingsSerializeAs: {0}", "SettingsSerializeAs"));
                }
                if (string.IsNullOrEmpty(_settingsSerializeAs))
                {
                    throw new ApplicationException(string.Format("Settings serialization format not set: '{0}'.\nCheck SettingsSerializeAs in App.config file.", _settingsSerializeAs));
                }
                //use with the filename
                SettingsBase.SerializeAs = SettingsBase.ToSerializationFormat(_settingsSerializeAs);

                //Second, override with passed values

                if ((ProgramBase.Filename != default) && (ProgramBase.Filename != SettingsController<MVCSettings>.FILE_NEW))
                {
                    //got filename from command line
                    SettingsController<MVCSettings>.Filename = ProgramBase.Filename;
                }
                if (ProgramBase.Directory != default)
                {
                    //get default directory from command line
                    SettingsController<MVCSettings>.Pathname = ProgramBase.Directory.WithTrailingSeparator();
                }
                if (ProgramBase.Format != default)
                {
                    // get default format from command line
                    SettingsBase.SerializeAs = ProgramBase.Format switch
                    {
                        "xml" => SettingsBase.SerializationFormat.Xml,
                        "json" => SettingsBase.SerializationFormat.Json,
                        _ => default
                    };
                }

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                //throw;
            }
            return returnValue;
        }

        private void BindSizeAndLocation()
        {
            //Note:Size must be done after InitializeComponent(); do Location this way as well.--SJS
            // this.DataBindings.Add(new System.Windows.Forms.Binding("Location", global::MVCForms.Properties.Settings.Default, "Location", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            // this.DataBindings.Add(new System.Windows.Forms.Binding("ClientSize", global::MVCForms.Properties.Settings.Default, "Size", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            Size =
                new Size
                (
					EtoForms.Properties.Settings.Default.Size.Width,
					EtoForms.Properties.Settings.Default.Size.Height
                );
            Location =
                new Point
                (
					EtoForms.Properties.Settings.Default.Location.X,
					EtoForms.Properties.Settings.Default.Location.Y
                );
        }
        #endregion Utility
        #endregion Methods

        #region Actions
        // Actions

        // private async Task DoSomething()
        // {
        //     for (int i = 0; i < 3; i++)
        //     {
        //         //progressBar.Pulse();
        //         //DoEvents;
        //         await Task.Delay(1000); 
        //     }
        // }

        #endregion Actions

        #region utility
        // private void StartProgressBar()
        // {
        //     progressBar.Value = 33;
        //     progressBar.Indeterminate = true;
        //     progressBar.Visible = true;
        //     //DoEvents;
        // }

        // private void StopProgressBar()
        // {
        //     //DoEvents;
        //     progressBar.Visible = false;
        // }

        // private void StartActionIcon(string resourceItemId)
        // {
        //     actionIcon.Image = imageResources[resourceItemId]; //"New", etc.
        //     actionIcon.Visible = true;
        // }

        // private void StopActionIcon()
        // {
        //     actionIcon.Visible = false;
        //     actionIcon.Image = imageResources["New"]; 
        // }

        // https://www.hanselman.com/blog/how-do-you-use-systemdrawing-in-net-core
        // private static Image GetImageFromFile(string resourceItemPath)
        // {
        //     Image returnValue = null;
        //     using (FileStream pngStream = new FileStream(resourceItemPath, FileMode.Open, FileAccess.Read))
        //     {
        //         Bitmap image = new Bitmap(pngStream);
        //         returnValue = image;
        //     }
        //     return returnValue;
        // }
        private static Bitmap GetBitmapFromFile(string resourceItemPath)
        {
            Bitmap returnValue = null;
            using (FileStream pngStream = new(resourceItemPath, FileMode.Open, FileAccess.Read))
            {
                returnValue = new Bitmap(pngStream);
            }
            return returnValue;
        }
        private static Icon GetIconFromBitmap(Bitmap bitmap, int width, int height)
        {
            return bitmap.WithSize(width, height);
        }

        #endregion utility

    }
}
