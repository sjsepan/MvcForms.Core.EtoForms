//Note: these should not be necessary in the code-behind partial-class of the front class defines them
//TODO:test to see if this is just omnisharp and not the compiler
using Eto.Forms;

namespace MvcForms.Core.EtoForms
{
	// The following keywords on a partial-type definition are optional, but 
	// if present on one partial-type definition, cannot conflict with the 
	// keywords specified on another partial definition for the same type: [...]
	// If any part declares a base type, then the whole type inherits that class.
	// All the parts that specify a base class must agree, but parts that omit a base class 
	// still inherit the base type. Parts can specify different base interfaces, 
	// and the final type implements all the interfaces listed by all the 
	// partial declarations. Any class, struct, or interface members declared in a 
	// partial definition are available to all the other parts. The final type is 
	// the combination of all the parts at compile time.
	// https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/partial-classes-and-methods
	/*public*/
	partial class MvcView //: Form //Note:access and base(s) not needed in in code-behind, as long as front class has it
	{
        //TODO: not sure how components gets used in WinForms, but it was required by designer
		// /// <summary>
        // /// Required designer variable.
        // /// </summary>
        // private System.ComponentModel.IContainer components = null;

        // /// <summary>
        // /// Clean up any resources being used.
        // /// </summary>
        // /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        // protected override void Dispose(bool disposing)
        // {
        //     if (disposing && (components != null))
        //     {
        //         components.Dispose();
        //     }
        //     base.Dispose(disposing);
        // }

        //if only...
		#region Form Designer generated code

		void InitializeComponent()
		{
			//Organize this like WinForms designer generated file <form>.designer.cs

			//Note: not sure if this will fit in, but mentioned for completeness
            // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MvcView));

            //Instantiate controls
            menuBar = new MenuBar();
			fileSubMenuItem = new SubMenuItem();
			editSubMenuItem = new SubMenuItem();
            windowSubMenuItem = new SubMenuItem();
			helpSubMenuItem = new SubMenuItem();
            toolBar = new ToolBar();
            fileNewButtonToolItem = new ButtonToolItem();
            fileOpenButtonToolItem = new ButtonToolItem();
            fileSaveButtonToolItem = new ButtonToolItem();
            filePrintButtonToolItem = new ButtonToolItem();
            separatorToolItem1 = new SeparatorToolItem();
            editUndoButtonToolItem = new ButtonToolItem();
            editRedoButtonToolItem = new ButtonToolItem();
            editCutButtonToolItem = new ButtonToolItem();
            editCopyButtonToolItem = new ButtonToolItem();
            editPasteButtonToolItem = new ButtonToolItem();
            editDeleteButtonToolItem = new ButtonToolItem();
            editFindButtonToolItem = new ButtonToolItem();
            editReplaceButtonToolItem = new ButtonToolItem();
            editRefreshButtonToolItem = new ButtonToolItem();
            editPreferencesButtonToolItem = new ButtonToolItem();
            separatorToolItem2 = new SeparatorToolItem();
            helpContentsButtonToolItem = new ButtonToolItem();
            fileNewCommand = new Command();
            fileOpenCommand = new Command();
            fileSaveCommand = new Command();
            fileSaveAsCommand = new Command();
            fileSeparatorMenuItem1 = new SeparatorMenuItem();
            filePrintCommand = new Command();
            filePrintPreviewCommand = new Command();
            fileQuitCommand = new Command();
            editUndoCommand = new Command();
            editRedoCommand = new Command();
            editSeparatorMenuItem1 = new SeparatorMenuItem();
            editSelectAllCommand = new Command();
            editCutCommand = new Command();
            editCopyCommand = new Command();
            editPasteCommand = new Command();
            editPasteSpecialCommand = new Command();
            editDeleteCommand = new Command();
            editSeparatorMenuItem2 = new SeparatorMenuItem();
            editFindCommand = new Command();
            editReplaceCommand = new Command();
            editSeparatorMenuItem3 = new SeparatorMenuItem();
            editRefreshCommand = new Command();
            editPreferencesCommand = new Command();
            editPropertiesCommand = new Command();
            windowNewWindowCommand = new Command();
            windowTileCommand = new Command();
            windowCascadeCommand = new Command();
            windowArrangeAllCommand = new Command();
            windowSeparatorMenuItem1 = new SeparatorMenuItem();
            windowHideCommand = new Command();
            windowShowCommand = new Command();
            helpContentsCommand = new Command();
            helpIndexCommand = new Command();
            helpOnlineHelpCommand = new Command();
            helpSeparatorMenuItem1 = new SeparatorMenuItem();
            helpLicenseInformationCommand = new Command();
            helpCheckForUpdatesCommand = new Command();
			helpAboutCommand = new Command();
            formContentStackLayout1 = new StackLayout();
            formContentStackLayoutItem1 = new StackLayoutItem();
            formContentTableLayout1 = new TableLayout(7, 6);
            lblSomeInt = new Label();
            lblSomeOtherInt = new Label();
            lblStillAnotherInt = new Label();
            lblSomeString = new Label();
            lblSomeOtherString = new Label();
            lblStillAnotherString  = new Label();
            lblSomeBoolean = new Label();
            lblSomeOtherBoolean = new Label();
            lblStillAnotherBoolean = new Label();
            txtSomeInt = new TextBox();
            txtSomeOtherInt = new TextBox();
            txtStillAnotherInt = new TextBox();
            txtSomeString = new TextBox();
            txtSomeOtherString = new TextBox();
            txtStillAnotherString = new TextBox();
            chkSomeBoolean = new CheckBox();
            chkSomeOtherBoolean = new CheckBox();
            chkStillAnotherBoolean = new CheckBox();
            cmdRun = new Button();
            formContentStackLayout2 = new StackLayout();
            formContentStackLayoutItem2 = new StackLayoutItem();
            formContentTableLayout2 = new TableLayout(5, 1);
            statusMessage = new Label();
            errorMessage = new Label();
            progressBar = new ProgressBar();
            actionIcon = new ImageView();
            dirtyIcon = new ImageView();
            cmdColor = new Button();
            cmdFont = new Button();
            // ToolBar.SuspendLayout();
            // Menu.SuspendLayout();
            // StatusBar.SuspendLayout();
            SuspendLayout();//Note:may not be needed/supported by Eto.Forms

            //Assign control properties

            //
            // cmdColor
            //
            cmdColor.Text = "Color";
            cmdColor.Click += CmdColor_Click;
            //
            // cmdFont
            //
            cmdFont.Text = "Font";
            cmdFont.Click += CmdFont_Click;
            //
            // lblSomeInt
            //
            lblSomeInt.Text = "Some Int: ";
            //
            //lblSomeOtherInt
            //
            lblSomeOtherInt.Text = "Some Other Int: ";
            //
            // lblStillAnotherInt
            //
            lblStillAnotherInt.Text = "Still Another Int: ";
            //
            // lblSomeString
            //
            lblSomeString.Text = "Some String: ";
            //
            // lblSomeOtherString
            //
            lblSomeOtherString.Text = "Some Other String: ";
            //
            // lblStillAnotherString 
            //
            lblStillAnotherString.Text = "Still Another String: ";
            //
            // lblSomeBoolean
            //
            lblSomeBoolean.Text = "Some Boolean: ";
            //
            // lblSomeOtherBoolean
            //
            lblSomeOtherBoolean.Text = "Some Other Boolean: ";
            //
            // lblStillAnotherBoolean
            //
            lblStillAnotherBoolean.Text = "Still Another Boolean: ";
            //
            // txtSomeInt
            //
            txtSomeInt.Text = "0";
            txtSomeInt.TextChanged += TxtSomeInt_TextChanged;
            //
            // txtSomeOtherInt
            //
            txtSomeOtherInt.Text = "0";
            txtSomeOtherInt.TextChanged += TxtSomeOtherInt_TextChanged;
            //
            // txtStillAnotherInt
            //
            txtStillAnotherInt.Text = "0";
            txtStillAnotherInt.TextChanged += TxtStillAnotherInt_TextChanged;
            //
            // txtSomeString
            //
            txtSomeString.Text = "";
            txtSomeString.TextChanged += TxtSomeString_TextChanged;
            //
            // txtSomeOtherString
            //
            txtSomeOtherString.Text = "";
            txtSomeOtherString.TextChanged += TxtSomeOtherString_TextChanged;
            //
            // txtStillAnotherString
            //
            txtStillAnotherString.Text = "";
            txtStillAnotherString.TextChanged += TxtStillAnotherString_TextChanged;
            //
            // chkSomeBoolean
            //
            chkSomeBoolean.Checked = false;
            chkSomeBoolean.CheckedChanged += ChkSomeBoolean_CheckChanged;
            //            
            // chkSomeOtherBoolean
            //
            chkSomeOtherBoolean.Checked = false;
            chkSomeOtherBoolean.CheckedChanged += ChkSomeOtherBoolean_CheckChanged;
            //
            // chkStillAnotherBoolean
            //
            chkStillAnotherBoolean.Checked = false;
            chkStillAnotherBoolean.CheckedChanged += ChkStillAnotherBoolean_CheckChanged;
            //
            // cmdRun
            //
            cmdRun.Text = "Run";
            cmdRun.Click += CmdRun_Clicked;//Any_Click;
            //
            // formContentTableLayout1
            //
            formContentTableLayout1.ID = "formContentTableLayout1";
            formContentTableLayout1.Add(lblSomeInt, 0, 0);
            formContentTableLayout1.Add(lblSomeOtherInt, 2, 0);
            formContentTableLayout1.Add(lblStillAnotherInt, 4, 0);
            formContentTableLayout1.Add(lblSomeString, 0, 1);
            formContentTableLayout1.Add(lblSomeOtherString, 2, 1);
            formContentTableLayout1.Add(lblStillAnotherString, 4, 1);
            formContentTableLayout1.Add(lblSomeBoolean, 0, 2);
            formContentTableLayout1.Add(lblSomeOtherBoolean, 2, 2);
            formContentTableLayout1.Add(lblStillAnotherBoolean, 4, 2);
            formContentTableLayout1.Add(txtSomeInt, 1, 0);
            formContentTableLayout1.Add(txtSomeOtherInt, 3, 0);
            formContentTableLayout1.Add(txtStillAnotherInt, 5, 0);
            formContentTableLayout1.Add(txtSomeString, 1, 1);
            formContentTableLayout1.Add(txtSomeOtherString, 3, 1);
            formContentTableLayout1.Add(txtStillAnotherString, 5, 1);
            formContentTableLayout1.Add(chkSomeBoolean, 1, 2);
            formContentTableLayout1.Add(chkSomeOtherBoolean, 3, 2);
            formContentTableLayout1.Add(chkStillAnotherBoolean, 5, 2);
            formContentTableLayout1.Add(cmdRun, 6, 0);
            formContentTableLayout1.Add(cmdColor, 0, 4);
            formContentTableLayout1.Add(cmdFont, 1, 4);
            //
            // formContentStackLayoutItem1
            //
            formContentStackLayoutItem1.Control = formContentTableLayout1;
            formContentStackLayoutItem1.Expand = true;
            //
            // statusMessage
            //
            statusMessage.Text = "";
            statusMessage.TextColor = Eto.Drawing.Color.FromArgb(94, 134, 16);
            //
            // errorMessage
            //
            errorMessage.Text = "";
            errorMessage.TextColor = Eto.Drawing.Color.FromArgb(255, 0, 0);
            //
            // progressBar
            //
            progressBar.Indeterminate = true;
            progressBar.MaxValue = 0;
            progressBar.MaxValue = 100;
            progressBar.Value = 33;
            progressBar.Visible = false;
            //
            // actionIcon
            // https://stackoverflow.com/questions/9901518/store-image-in-resx-as-byte-rather-than-bitmap
            // actionIcon.Image = Eto.Drawing.Bitmap.FromResource("MvcForms.Core.EtoForms.Resources.New.png");
            actionIcon.Image = imageResources["New"];
            actionIcon.ToolTip = "New";
            actionIcon.Visible = false;
            //
            // dirtyIcon
            //
            dirtyIcon.Image = imageResources["Save"];
            dirtyIcon.ToolTip = "Dirty";
            dirtyIcon.Visible = false;
            //
            // formContentTableLayout2
            //
            formContentTableLayout2.ID = "formContentTableLayout2";
            formContentTableLayout2.Add(statusMessage, 0, 0);
            formContentTableLayout2.Add(errorMessage, 1, 0);
            formContentTableLayout2.Add(progressBar, 2, 0);
            formContentTableLayout2.Add(actionIcon, 3, 0);
            formContentTableLayout2.Add(dirtyIcon, 4, 0);
            //
            // formContentStackLayoutItem2
            //
            formContentStackLayoutItem2.Control = formContentTableLayout2;
            formContentStackLayoutItem2.Expand = true;
            //
            // formContentStackLayout2
            //
            formContentStackLayout2.Orientation = Orientation.Horizontal;
            formContentStackLayout2.Items.Add(formContentStackLayoutItem2);
            //
            // formContentStackLayout1
            //
            formContentStackLayout1.Items.Add(formContentStackLayoutItem1);
            formContentStackLayout1.Items.Add(formContentStackLayout2);
            //
            // fileNewCommand
            //
            fileNewCommand.MenuText = "&New";
            fileNewCommand.ToolTip = "New";
            fileNewCommand.Image = imageResources["New"];
            fileNewCommand.Shortcut = Application.Instance.CommonModifier | Keys.N;
            fileNewCommand.Executed += MenuFileNew_Click;
            //
            // fileOpenCommand
            //
            fileOpenCommand.MenuText = "&Open";
            fileOpenCommand.Image = imageResources["Open"];
            fileOpenCommand.Shortcut = Application.Instance.CommonModifier | Keys.O;
            fileOpenCommand.Executed += MenuFileOpen_Click;
            //
            // fileSaveCommand
            //
            fileSaveCommand.MenuText = "&Save";
            fileSaveCommand.Image = imageResources["Save"];
            fileSaveCommand.Shortcut = Application.Instance.CommonModifier | Keys.S;
            fileSaveCommand.Executed += MenuFileSave_Click;
            //
            // fileSaveAsCommand
            //
            fileSaveAsCommand.MenuText = "Save &As...";
            // fileSaveAsCommand.Image = imageResources["Save"];
            // fileSaveAsCommand.Shortcut = Application.Instance.CommonModifier | Keys.Z;
            fileSaveAsCommand.Executed += MenuFileSaveAs_Click;
            //
            // filePrintCommand
            //
            filePrintCommand.MenuText = "&Print";
            filePrintCommand.Image = imageResources["Print"];
            filePrintCommand.Shortcut = Application.Instance.CommonModifier | Keys.P;
            filePrintCommand.Executed += MenuFilePrint_Click;
            //
            // filePrintPreviewCommand
            //
            filePrintPreviewCommand.MenuText = "P&rint Preview...";
            // filePrintPreviewCommand.Image = imageResources["Print"];
            // filePrintPreviewCommand.Shortcut = Application.Instance.CommonModifier | Keys.Z;
            filePrintPreviewCommand.Executed += MenuFilePrintPreview_Click;
            // 
            // fileQuitCommand
            // 
            fileQuitCommand.MenuText = "&Quit";
            //fileQuitCommand.Image = imageResources["Quit"];
            fileQuitCommand.Shortcut = Application.Instance.CommonModifier | Keys.Q;
            fileQuitCommand.Executed += MenuFileQuit_Click;
            //
            // editUndoCommand
            //
            editUndoCommand.MenuText = "&Undo";
            editUndoCommand.Image = imageResources["Undo"];
            editUndoCommand.Shortcut = Application.Instance.CommonModifier | Keys.Z;
            editUndoCommand.Executed += MenuEditUndo_Click;
            //
            // editRedoCommand
            //
            editRedoCommand.MenuText = "&Redo";
            editRedoCommand.Image = imageResources["Redo"];
            editRedoCommand.Shortcut = Application.Instance.CommonModifier | Keys.Y;
            editRedoCommand.Executed += MenuEditRedo_Click;
            //
            // editSelectAllCommand
            //
            editSelectAllCommand.MenuText = "Select All";
            // editSelectAllCommand.Image = imageResources["SelectAll"];
            editSelectAllCommand.Shortcut = Application.Instance.CommonModifier | Keys.A;
            editSelectAllCommand.Executed += MenuEditSelectAll_Click;
            //
            // editCutCommand
            //
            editCutCommand.MenuText = "Cu&t";
            editCutCommand.Image = imageResources["Cut"];
            editCutCommand.Shortcut = Application.Instance.CommonModifier | Keys.X;
            editCutCommand.Executed += MenuEditCut_Click;
            //
            // editCopyCommand
            //
            editCopyCommand.MenuText = "&Copy";
            editCopyCommand.Image = imageResources["Copy"];
            editCopyCommand.Shortcut = Application.Instance.CommonModifier | Keys.C;
            editCopyCommand.Executed += MenuEditCopy_Click;
            //
            // editPasteCommand
            //
            editPasteCommand.MenuText = "&Paste";
            editPasteCommand.Image = imageResources["Paste"];
            editPasteCommand.Shortcut = Application.Instance.CommonModifier | Keys.V;
            editPasteCommand.Executed += MenuEditPaste_Click;
            //
            // editPasteSpecialCommand
            //
            editPasteSpecialCommand.MenuText = "Paste &Special...";
            // editPasteSpecialCommand.Image = imageResources["PasteSpecial"];
            // editPasteSpecialCommand.Shortcut = Application.Instance.CommonModifier | Keys.Z;
            editPasteSpecialCommand.Executed += MenuEditPasteSpecial_Click;
            //
            // editDeleteCommand
            //
            editDeleteCommand.MenuText = "&Delete";
            editDeleteCommand.Image = imageResources["Delete"];
            editDeleteCommand.Shortcut = Keys.Clear;
            editDeleteCommand.Executed += MenuEditDelete_Click;
            //
            // editFindCommand
            //
            editFindCommand.MenuText = "F&ind...";
            editFindCommand.Image = imageResources["Find"];
            editFindCommand.Shortcut = Application.Instance.CommonModifier | Keys.F;
            editFindCommand.Executed += MenuEditFind_Click;
            //
            // editReplaceCommand
            //
            editReplaceCommand.MenuText = "R&eplace...";
            editReplaceCommand.Image = imageResources["Replace"];
            editReplaceCommand.Shortcut = Application.Instance.CommonModifier | Keys.H;
            editReplaceCommand.Executed += MenuEditReplace_Click;
            //
            // editRefreshCommand
            //
            editRefreshCommand.MenuText = "Re&fresh";
            editRefreshCommand.Image = imageResources["Refresh"];
            editRefreshCommand.Shortcut = Keys.F5;
            editRefreshCommand.Executed += MenuEditRefresh_Click;
            //
            // editPreferencesCommand
            //
            editPreferencesCommand.MenuText = "Prefere&nces...";
            editPreferencesCommand.Image = imageResources["Preferences"];
            // editPreferencesCommand.Shortcut = Application.Instance.CommonModifier | Keys.Z;
            editPreferencesCommand.Executed += MenuEditPreferences_Click;
            //
            // editPropertiesCommand
            //
            editPropertiesCommand.MenuText = "Proper&ties...";
            editPropertiesCommand.Image = imageResources["Properties"];
            // editPropertiesCommand.Shortcut = Application.Instance.CommonModifier | Keys.Z;
            editPropertiesCommand.Executed += MenuEditProperties_Click;
            //
            // windowNewWindowCommand
            //
            windowNewWindowCommand.MenuText = "&New Window";
            // windowNewWindowCommand.Image = imageResources["NewWindow"];
            // windowNewWindowCommand.Shortcut = Application.Instance.CommonModifier | Keys.Z;
            windowNewWindowCommand.Executed += MenuWindowNewWindow_Click;
            //
            // windowTileCommand
            //
            windowTileCommand.MenuText = "&Tile";
            // windowTileCommand.Image = imageResources["Tile"];
            // windowTileCommand.Shortcut = Application.Instance.CommonModifier | Keys.Z;
            windowTileCommand.Executed += MenuWindowTile_Click;
            //
            // windowCascadeCommand
            //
            windowCascadeCommand.MenuText = "&Cascade";
            // windowCascadeCommand.Image = imageResources["Cascade"];
            // windowCascadeCommand.Shortcut = Application.Instance.CommonModifier | Keys.Z;
            windowCascadeCommand.Executed += MenuWindowCascade_Click;
            //
            // windowArrangeAllCommand
            //
            windowArrangeAllCommand.MenuText = "&Arrange All";
            // windowArrangeAllCommand.Image = imageResources["ArrangeAll"];
            // windowArrangeAllCommand.Shortcut = Application.Instance.CommonModifier | Keys.Z;
            windowArrangeAllCommand.Executed += MenuWindowArrangeAll_Click;
            //
            // windowHideCommand
            //
            windowHideCommand.MenuText = "&Hide";
            // windowHideCommand.Image = imageResources["Hide"];
            // windowHideCommand.Shortcut = Application.Instance.CommonModifier | Keys.Z;
            windowHideCommand.Executed += MenuWindowHide_Click;
            //
            // windowShowCommand
            //
            windowShowCommand.MenuText = "&Show";
            // windowShowCommand.Image = imageResources["Show"];
            // windowShowCommand.Shortcut = Application.Instance.CommonModifier | Keys.Z;
            windowShowCommand.Executed += MenuWindowShow_Click;
            //
            // helpContentsCommand
            //
            helpContentsCommand.MenuText = "&Contents";
            helpContentsCommand.Image = imageResources["Contents"];
            helpContentsCommand.Shortcut = Keys.F1;
            helpContentsCommand.Executed += MenuHelpContents_Click;
            //
            // helpIndexCommand
            //
            helpIndexCommand.MenuText = "&Index";
            // helpIndexCommand.Image = imageResources["Index"];
            // helpIndexCommand.Shortcut = Application.Instance.CommonModifier | Keys.Z;
            helpIndexCommand.Executed += MenuHelpIndex_Click;
            //
            // helpOnlineHelpCommand
            //
            helpOnlineHelpCommand.MenuText = "&Online Help";
            // helpOnlineHelpCommand.Image = imageResources["OnlineHelp"];
            // helpOnlineHelpCommand.Shortcut = Application.Instance.CommonModifier | Keys.Z;
            helpOnlineHelpCommand.Executed += MenuHelpOnlineHelp_Click;
            //
            // helpLicenseInformationCommand
            //
            helpLicenseInformationCommand.MenuText = "&License Information";
            // helpLicenseInformationCommand.Image = imageResources["LicenseInformation"];
            // helpLicenseInformationCommand.Shortcut = Application.Instance.CommonModifier | Keys.Z;
            helpLicenseInformationCommand.Executed += MenuHelpLicenceInformation_Click;
            //
            // helpCheckForUpdatesCommand
            //
            helpCheckForUpdatesCommand.MenuText = "Check For &Updates";
            // helpCheckForUpdatesCommand.Image = imageResources["CheckForUpdates"];
            // helpCheckForUpdatesCommand.Shortcut = Application.Instance.CommonModifier | Keys.Z;
            helpCheckForUpdatesCommand.Executed += MenuHelpCheckForUpdates_Click;
            // 
            // helpAboutCommand
            // 
            helpAboutCommand.MenuText = "&About...";
            helpAboutCommand.Image = imageResources["About"];
            // helpAboutCommand.Shortcut = Application.Instance.CommonModifier | Keys.Z;
            helpAboutCommand.Executed += MenuHelpAbout_Click;
            //
            // fileSubMenuItem
            //
            fileSubMenuItem.Text = "&File";
            fileSubMenuItem.Items.Add(fileNewCommand);
            fileSubMenuItem.Items.Add(fileOpenCommand);
            fileSubMenuItem.Items.Add(fileSaveCommand);
            fileSubMenuItem.Items.Add(fileSaveAsCommand);
            fileSubMenuItem.Items.Add(fileSeparatorMenuItem1);
            fileSubMenuItem.Items.Add(filePrintCommand);
            fileSubMenuItem.Items.Add(filePrintPreviewCommand);
            //Note:FileQuit added to MenuBar.QuitItem under built-in separator
            //
            // editSubMenuItem
            //
            editSubMenuItem.Text = "&Edit";
            editSubMenuItem.Items.Add(editUndoCommand);
            editSubMenuItem.Items.Add(editRedoCommand);
            editSubMenuItem.Items.Add(editSeparatorMenuItem1);
            editSubMenuItem.Items.Add(editSelectAllCommand);
            editSubMenuItem.Items.Add(editCutCommand);
            editSubMenuItem.Items.Add(editCopyCommand);
            editSubMenuItem.Items.Add(editPasteCommand);
            editSubMenuItem.Items.Add(editPasteSpecialCommand);
            editSubMenuItem.Items.Add(editDeleteCommand);
            editSubMenuItem.Items.Add(editSeparatorMenuItem2);
            editSubMenuItem.Items.Add(editFindCommand);
            editSubMenuItem.Items.Add(editReplaceCommand);
            editSubMenuItem.Items.Add(editSeparatorMenuItem3);
            editSubMenuItem.Items.Add(editRefreshCommand);
            editSubMenuItem.Items.Add(editSeparatorMenuItem3);
            editSubMenuItem.Items.Add(editPreferencesCommand);
            editSubMenuItem.Items.Add(editPropertiesCommand);
            //
            // windowSubMenuItem
            //
            windowSubMenuItem.Text = "&Window";
            windowSubMenuItem.Items.Add(windowNewWindowCommand);
            windowSubMenuItem.Items.Add(windowTileCommand);
            windowSubMenuItem.Items.Add(windowCascadeCommand);
            windowSubMenuItem.Items.Add(windowArrangeAllCommand);
            windowSubMenuItem.Items.Add(windowSeparatorMenuItem1);
            windowSubMenuItem.Items.Add(windowHideCommand);
            windowSubMenuItem.Items.Add(windowShowCommand);
            //
            // helpSubMenuItem
            //
            helpSubMenuItem.Text = "&Help";
            helpSubMenuItem.Items.Add(helpContentsCommand);
            helpSubMenuItem.Items.Add(helpIndexCommand);
            helpSubMenuItem.Items.Add(helpOnlineHelpCommand);
            helpSubMenuItem.Items.Add(helpSeparatorMenuItem1);
            helpSubMenuItem.Items.Add(helpLicenseInformationCommand);
            helpSubMenuItem.Items.Add(helpCheckForUpdatesCommand);
            //Note:HelpAbout added to MenuBar.AboutItem under built-in separator
            //
            // menuBar
            //
            menuBar.Items.Add(fileSubMenuItem);
            menuBar.Items.Add(editSubMenuItem);
            menuBar.Items.Add(windowSubMenuItem);
            menuBar.Items.Add(helpSubMenuItem);
            //menuBar.ApplicationItems.Add(editPreferencesButtonMenuItem);
            menuBar.QuitItem = fileQuitCommand;
            menuBar.AboutItem = helpAboutCommand;
            //
            // fileNewButtonToolItem
            //
            fileNewButtonToolItem.ToolTip = "New";
            fileNewButtonToolItem.Image = fileNewCommand.Image;//command Image not picked up automatically by Item
            fileNewButtonToolItem.Command = fileNewCommand;
            //
            // fileOpenButtonToolItem
            //
            fileOpenButtonToolItem.ToolTip = "Open";
            fileOpenButtonToolItem.Image = fileOpenCommand.Image;
            fileOpenButtonToolItem.Command = fileOpenCommand;
            //
            // fileSaveButtonToolItem
            //
            fileSaveButtonToolItem.ToolTip = "Save";
            fileSaveButtonToolItem.Image = fileSaveCommand.Image;
            fileSaveButtonToolItem.Command = fileSaveCommand;
            //
            // filePrintButtonToolItem
            //
            filePrintButtonToolItem.ToolTip = "Print";
            filePrintButtonToolItem.Image = filePrintCommand.Image;
            filePrintButtonToolItem.Command = filePrintCommand;
            //
            // separatorToolItem1
            //
            separatorToolItem1.ID = "separatorToolItem1";
            //
            // editUndoButtonToolItem
            //
            editUndoButtonToolItem.ToolTip = "Undo";
            editUndoButtonToolItem.Image = editUndoCommand.Image;
            editUndoButtonToolItem.Command = editUndoCommand;
            //
            // editRedoButtonToolItem
            //
            editRedoButtonToolItem.ToolTip = "Redo";
            editRedoButtonToolItem.Image = editRedoCommand.Image;
            editRedoButtonToolItem.Command = editRedoCommand;
            //
            // editCutButtonToolItem
            //
            editCutButtonToolItem.ToolTip = "Cut";
            editCutButtonToolItem.Image = editCutCommand.Image;
            editCutButtonToolItem.Command = editCutCommand;
            //
            // editCopyButtonToolItem
            //
            editCopyButtonToolItem.ToolTip = "Copy";
            editCopyButtonToolItem.Image = editCopyCommand.Image;
            editCopyButtonToolItem.Command = editCopyCommand;
            //
            // editPasteButtonToolItem
            //
            editPasteButtonToolItem.ToolTip = "Paste";
            editPasteButtonToolItem.Image = editPasteCommand.Image;
            editPasteButtonToolItem.Command = editPasteCommand;
            //
            // editDeleteButtonToolItem
            //
            editDeleteButtonToolItem.ToolTip = "Delete";
            editDeleteButtonToolItem.Image = editDeleteCommand.Image;
            editDeleteButtonToolItem.Command = editDeleteCommand;
            //
            // editfindButtonToolItem
            //
            editFindButtonToolItem.ToolTip = "Find";
            editFindButtonToolItem.Image = editFindCommand.Image;
            editFindButtonToolItem.Command = editFindCommand;
            //
            // editReplaceButtonToolItem
            //
            editReplaceButtonToolItem.ToolTip = "Replace";
            editReplaceButtonToolItem.Image = editReplaceCommand.Image;
            editReplaceButtonToolItem.Command = editReplaceCommand;
            //
            // editRefreshButtonToolItem
            //
            editRefreshButtonToolItem.ToolTip = "Refresh";
            editRefreshButtonToolItem.Image = editRefreshCommand.Image;
            editRefreshButtonToolItem.Command = editRefreshCommand;
            //
            // editPreferencesButtonToolItem
            //
            editPreferencesButtonToolItem.ToolTip = "Preferences";
            editPreferencesButtonToolItem.Image = editPreferencesCommand.Image;
            editPreferencesButtonToolItem.Command = editPreferencesCommand;
            //
            // separatorToolItem2
            //
            separatorToolItem2.ID = "separatorToolItem2";
            //
            // helpAboutButtonToolItem
            //
            helpContentsButtonToolItem.ToolTip = "Contents";
            helpContentsButtonToolItem.Image = imageResources["Contents"];
            helpContentsButtonToolItem.Command = helpContentsCommand;
            // 
            // toolBar
            // Note: adding command here instead of ButtonToolItem gives warning "(MvcForms.Core.EtoForms.Gtk:29033): GLib-CRITICAL **: 10:57:46.633: Source ID 11 was not found when attempting to remove it"
            // https://stackoverflow.com/questions/23199699/glib-critical-source-id-xxx-was-not-found-when-attempting-to-remove-it
            // https://bugs.launchpad.net/ubuntu/+source/gnome-control-center/+bug/1264368
            toolBar.Items.Add(fileNewButtonToolItem);
            toolBar.Items.Add(fileOpenButtonToolItem);
            toolBar.Items.Add(fileSaveButtonToolItem);
            toolBar.Items.Add(filePrintButtonToolItem);
            toolBar.Items.Add(separatorToolItem1);
            toolBar.Items.Add(editUndoButtonToolItem);
            toolBar.Items.Add(editRedoButtonToolItem);
            toolBar.Items.Add(editCutButtonToolItem);
            toolBar.Items.Add(editCopyButtonToolItem);
            toolBar.Items.Add(editPasteButtonToolItem);
            toolBar.Items.Add(editDeleteButtonToolItem);
            toolBar.Items.Add(editFindButtonToolItem);
            toolBar.Items.Add(editReplaceButtonToolItem);
            toolBar.Items.Add(editRefreshButtonToolItem);
            toolBar.Items.Add(editPreferencesButtonToolItem);
            toolBar.Items.Add(separatorToolItem2);
            toolBar.Items.Add(helpContentsButtonToolItem);
            // 
            // MVCView
            // 
            Content = formContentStackLayout1;
            ToolBar = toolBar;
            Menu = menuBar;
            Title = "MvcForms.Core.EtoForms";
            Tag = "MvcView";
            ID = "MvcView";
			MinimumSize = new Eto.Drawing.Size(640, 480);
            ClientSize = new Eto.Drawing.Size(640, 480);
            Padding = 10;
            Load += MvcView_Load;
            Closing += MvcView_Closing;
            // ToolBar.ResumeLayout(false);
            // ToolBar.PerformLayout();
            // Menu.ResumeLayout(false);
            // Menu.PerformLayout();
            // StatusBar.ResumeLayout(false);
            // StatusBar.PerformLayout();
            // ResumeLayout(false);
            // PerformLayout();

        }

        #endregion

        //Declare Controls down here
        private MenuBar menuBar;
        private SubMenuItem fileSubMenuItem;
        private SubMenuItem editSubMenuItem;
        private SubMenuItem windowSubMenuItem;
        private SubMenuItem helpSubMenuItem;
        private ToolBar toolBar;
        private ButtonToolItem fileNewButtonToolItem;
        private ButtonToolItem fileOpenButtonToolItem;
        private ButtonToolItem fileSaveButtonToolItem;
        private ButtonToolItem filePrintButtonToolItem;
        private SeparatorToolItem separatorToolItem1;
        private ButtonToolItem editUndoButtonToolItem;
        private ButtonToolItem editRedoButtonToolItem;
        private ButtonToolItem editCutButtonToolItem;
        private ButtonToolItem editCopyButtonToolItem;
        private ButtonToolItem editPasteButtonToolItem;
        private ButtonToolItem editDeleteButtonToolItem;
        private ButtonToolItem editFindButtonToolItem;
        private ButtonToolItem editReplaceButtonToolItem;
        private ButtonToolItem editRefreshButtonToolItem;
        private ButtonToolItem editPreferencesButtonToolItem;
        private SeparatorToolItem separatorToolItem2;
        private ButtonToolItem helpContentsButtonToolItem;
        private Command fileNewCommand;
        private Command fileOpenCommand;
        private Command fileSaveCommand;
        private Command fileSaveAsCommand;
        private SeparatorMenuItem fileSeparatorMenuItem1;
        private Command filePrintCommand;
        private Command filePrintPreviewCommand;
        private Command fileQuitCommand;
        private Command editUndoCommand;
        private Command editRedoCommand;
        private SeparatorMenuItem editSeparatorMenuItem1;
        private Command editSelectAllCommand;
        private Command editCutCommand;
        private Command editCopyCommand;
        private Command editPasteCommand;
        private Command editPasteSpecialCommand;
        private Command editDeleteCommand;
        private SeparatorMenuItem editSeparatorMenuItem2;
        private Command editFindCommand;
        private Command editReplaceCommand;
        private SeparatorMenuItem editSeparatorMenuItem3;
        private Command editRefreshCommand;
        private Command editPreferencesCommand;
        private Command editPropertiesCommand;
        private Command windowNewWindowCommand;
        private Command windowTileCommand;
        private Command windowCascadeCommand;
        private Command windowArrangeAllCommand;
        private SeparatorMenuItem windowSeparatorMenuItem1;
        private Command windowHideCommand;
        private Command windowShowCommand;
        private Command helpContentsCommand;
        private Command helpIndexCommand;
        private Command helpOnlineHelpCommand;
        private SeparatorMenuItem helpSeparatorMenuItem1;
        private Command helpLicenseInformationCommand;
        private Command helpCheckForUpdatesCommand;
        private Command helpAboutCommand;
        private StackLayout formContentStackLayout1;
        private StackLayoutItem formContentStackLayoutItem1;
        private TableLayout formContentTableLayout1;
        private Label lblSomeInt;
        private Label lblSomeOtherInt;
        private Label lblStillAnotherInt;
        private Label lblSomeString;
        private Label lblSomeOtherString;
        private Label lblStillAnotherString;
        private Label lblSomeBoolean;
        private Label lblSomeOtherBoolean;
        private Label lblStillAnotherBoolean;
        private TextBox txtSomeInt;
        private TextBox txtSomeOtherInt;
        private TextBox txtStillAnotherInt;
        private TextBox txtSomeString;
        private TextBox txtSomeOtherString;
        private TextBox txtStillAnotherString;
        private CheckBox chkSomeBoolean;
        private CheckBox chkSomeOtherBoolean;
        private CheckBox chkStillAnotherBoolean;
        private Button cmdRun;
		private StackLayout formContentStackLayout2;
        private StackLayoutItem formContentStackLayoutItem2;
        private TableLayout formContentTableLayout2;
        private Label statusMessage;
        private Label errorMessage;
        private ProgressBar progressBar;
        private ImageView actionIcon;
        private ImageView dirtyIcon;
        private Button cmdColor;
        private Button cmdFont;
    }
}
