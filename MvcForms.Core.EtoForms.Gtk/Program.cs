﻿using System;
using System.Reflection;
using Ssepan.Application.Core;
using Ssepan.Utility.Core;
using Eto.Forms;

namespace MvcForms.Core.EtoForms.Gtk
{
	public class Program :
        ProgramBase //made ProgramBase for similar Program classes
	{
        #region Methods
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args">string[]</param>
        /// <returns>int</returns>
		[STAThread]
		public static int Main(string[] args)
		{
            //default to fail code
            int returnValue = -1;

            try
            {
                //define default output delegate
                ConsoleApplication.DefaultOutputDelegate = ConsoleApplication.writeLineWrapperOutputDelegate;

                //subscribe to notifications
                PropertyChanged += PropertyChangedEventHandlerDelegate;

                //load, parse, run switches
                DoSwitches(args);

				//UI-specific
                new Application(Eto.Platforms.Gtk).Run(new MvcView());

                //return success code
                returnValue = 0;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
		}
        #endregion Methods
	}
}
