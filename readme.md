# readme.md - README for MvcForms.Core.EtoForms

## Desktop GUI app demo, on Linux(/Win/Mac), in C# / DotNet[8] / Gtk\#(/WPF/AppKit), using VSCode / Eto.Forms

![MvcForms.Core.EtoForms_Gtk.png](./MvcForms.Core.EtoForms_Gtk.png?raw=true "Screenshot")

### About

See readme for project EtoFormsApp2 (<https://gitlab.com/sjsepan/EtoFormsApp2>) for discussion about why I chose to implement that project (and this one) with the 'preview' code option of the 'etoapp' template.

### Usage notes

~This application uses keys in app.config to identify a settings file to automatically load, for manual processing and saving. 1) The file name is given, and the format given must be one of two that the app library Ssepan.Application.Core knows how to serialize / deserialize: json, xml. 2) The value used must correspond to the format expected by the SettingsController in Ssepan.Application.Core. It is specified with the SerializeAs property of type SerializationFormat enum. 3) The file must reside in the user's personal directory, which on Linux is /home/username. A sample file can be found in the MvcLibrary.Core project in the Sample folder.
~The automatic loading is not necessary (it is a carry-over from the original console app) and can be disabled by editing the code in InitViewModel() in MvcView.cs to remove the ViewModel.FileOpen() call.

### Instructions for downloading/installing .Net 'Core'

<https://dotnet.microsoft.com/download>
<https://docs.microsoft.com/en-us/dotnet/core/install/linux?WT.mc_id=dotnet-35129-website>

### Install Eto.Forms templates for .Net Core

dotnet new --install Eto.Forms.Templates
Add <https://www.nuget.org/packages/Eto.Forms.Templates/>
Adds etoapp, etofile

### Getting Started

dotnet add package Eto.Forms --version 2.8.2

<https://github.com/picoe/Eto/wiki/Quick-Start>
dotnet new etoapp -sln -m xaml
dotnet new etoapp -sln -m json
dotnet new etoapp -sln -m code
dotnet new etoapp -sln -m preview

### Linux (Ubuntu): Get System.Drawing.Common features

sudo apt install libc6-dev
sudo apt install libgdiplus

### Install package for app configuration

~in folder for project MvcForms.Core.EtoForms use:
dotnet add package System.Configuration.ConfigurationManager

### Build / Run from terminal

If you try to run ('dotnet run') from MvcForms.Core.EtoForms project folder, you will get an error "The current OutputType is 'Library'". To run MvcForms.Core.EtoForms.Gtk, for example, make sure you run from the MvcForms.Core.EtoForms.Gtk folder.

### Tutorials

<https://github.com/picoe/Eto/wiki/Tutorials>
<https://www.hanselman.com/blog/crossplatform-guis-with-open-source-net-using-etoforms>

### Notes

Note: adding a command directly to ToolBar instead of adding a ButtonToolItem using that command gives warning "(MvcForms.Core.EtoForms.Gtk:29033): GLib-CRITICAL **: 10:57:46.633: Source ID 11 was not found when attempting to remove it"
<https://stackoverflow.com/questions/23199699/glib-critical-source-id-xxx-was-not-found-when-attempting-to-remove-it>
<https://bugs.launchpad.net/ubuntu/+source/gnome-control-center/+bug/1264368>

### Enhancements

0.11.2:
~Update Eto.Forms.* to 2.8.3

0.11.1:
~correct readme versions
~finish hints and command-line args code in view

0.11:
~refactor to newer C# features
~refactor to existing language types
~perform format linting
~redo command-line args and how passed filename overrides config file settings, including addition of a format arg.

0.10:
~Update dotnet to .net8.0
~Update Eto.Forms.* to 2.8.2

0.9:
-update projects to net7.0
-update Eto.Platform.* nuget to 2.7.4

0.8:
~Initial build, modeled after Gtk# demo.

### Contact

Steve Sepan
<sjsepan@yahoo.com>
3/8/2024
